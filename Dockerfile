FROM node
WORKDIR /usr/src/app
COPY package.json .
RUN npm install
COPY . .
RUN ng build --prod --build-optimizer
EXPOSE 3001
CMD [ "node", "index.js"]


